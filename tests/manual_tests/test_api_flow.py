import requests

# The base URL for the API
BASE_URL = "http://127.0.0.1:5000"


def create_task(title, description, priority="LOW"):
    """
    Creates a task using the web API.
    """
    response = requests.post(
        f"{BASE_URL}/tasks",
        json={"title": title, "description": description, "priority": priority},
    )
    return response


def mark_task_complete(task_id: int):
    """
    Marks a task complete using the web API.
    """
    response = requests.post(
        f"{BASE_URL}/tasks/complete/{task_id}")
    return response


def list_tasks():
    """
    Lists all tasks using the web API.
    """
    response = requests.get(f"{BASE_URL}/tasks")
    return response


def main():
    # Creating two tasks
    print("Creating Task 1...")
    create_task("Buy groceries", "Milk, Eggs, Bread")
    print("Task 1 created.")

    print("Creating Task 2...")
    create_task("Prepare meeting", "Slides for the project update", priority="MEDIUM")
    print("Task 2 created.")

    print("Creating Task 3...")
    create_task("Production Issue", "API failing in prod", priority="HIGH")
    print("Task 3 created.")

    print("Creating Task 4...")
    create_task("Rotate Secret in AWS", "secret expiring for API", priority="HIGH")
    print("Task 4 created.")

    print("Completing Task 4 of HIGH priority...")
    mark_task_complete(4)
    print("Task 4 completed.")

    # Listing all tasks
    print("Listing all tasks...")
    response = list_tasks()
    tasks = response.json()
    print("Tasks listed:")
    for task in tasks:
        print(
            f"ID: {task['id']}, Title: {task['title']}, Priority: {task['priority']}, Completed: {task['is_completed']}"
        )


if __name__ == "__main__":
    main()
